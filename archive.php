<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

get_header(); ?>
	<article id="archive">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-11">
                        <h1>
		                    <?php wp_title( '' ); ?>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-11">
	                    <?php
	                    if ( have_posts() ) : ?>
		                    <?php
		                    while ( have_posts() ) : the_post();
			                    get_template_part( 'template-parts/content', get_post_format() );
		                    endwhile;
	                    else :
		                    get_template_part( 'template-parts/content', 'none' );
	                    endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </article>
<?php
get_footer();
