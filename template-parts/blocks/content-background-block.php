<?php
$content = get_field('content_background_block');
?>

<?php if (!empty($content)): ?>
    <div class="content-background-block">
        <div class="content-background-block__container">
            <div class="title-quote-block__content">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
<?php endif; ?>