<?php
$args = array(
    'numberposts' => -1,
    'post_type' => 'partners',
    'orderby' => 'title',
    'order' => 'ASC',
);
// Get the posts
$partnersGlobal = get_posts($args);

if (empty($partners)) {
    $partners = $partnersGlobal;
} else {
    $partners = get_field('partners');
}

if ($partners): ?>
    <div class="content-partners-block">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="content-partners-block__title">
                        <span>
                            <?php echo __('Onze partners', 'webcommitment-theme'); ?>
                        </span>
                    </div>

                    <div class="splide">
                        <div class="splide__track">
                            <ul class="splide__list">
                                <?php foreach ($partners as $partnerId):
                                    // Setup this post for WP functions (variable must be named $partner).
                                    setup_postdata($partnerId); ?>
                                    <div class="splide__slide">
                                        <a href="<?php the_field('hyperlink', $partnerId); ?>"
                                           target="_blank"
                                           rel="<?php echo get_the_title($partnerId); ?>">
                                            <?php echo get_the_post_thumbnail($partnerId, 'wc-logo-block'); ?>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>
<?php endif; ?>



