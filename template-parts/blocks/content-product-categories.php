<?php
$categories = get_field('select_categories');
?>

<?php if (!empty($categories)): ?>

    <div class="product-categories-block">
        <?php foreach ($categories as $category): ?>
            <div class="product-categories-block__item">
                <?php
                $thumbnail_id = get_term_meta($category, 'thumbnail_id', true);

                $slug = get_term($category)->slug;
                $activiteiten_link = '';
                if ($slug == 'kinderen'):
                    $activiteiten_link = get_field('kinderen_activiteiten_pagina', 'option');
                else:
                    $activiteiten_link = get_field('volwassenen_activiteiten_pagina', 'option');
                endif;

                $image_url = wp_get_attachment_url($thumbnail_id);
                ?>
                <a href="<?php echo get_permalink($activiteiten_link); ?>"
                    aria-label="category"
                   class="product-categories-block__item--block"
                >
                    <?php
                    $category = get_the_category_by_ID($category);
                    $color = 'orange';
                    $category == 'Kinderen' ? $color = 'orange' : $color = 'purple';

                    ?>
                    <div class="product-categories-block__item--title cta-btn cta-btn__<?php echo $color; ?>"
                    >
                        <?php print_r($category); ?>
                    </div>
                    <div class="product-categories-block__overlay"
                         style="background-image: url('<?php echo $image_url; ?>')"
                    ></div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>

<?php endif; ?>