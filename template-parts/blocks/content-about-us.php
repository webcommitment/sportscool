<?php
$logo_left = get_field('about_logo_left');
$title = get_field('about_title');
$content = get_field('about_content');
$logo_right = get_field('about_logo_right');
?>

<?php if ($content || $title): ?>
    <div class="about-us-block">
        <div class="about-us-block__row">
            <?php if (!empty($logo_left)): ?>
                <div class="about-us-block__logo about-us-block__logo--left">
                    <img src="<?php echo $logo_left['sizes']['wc-logo-block']; ?>"
                         alt="<?php echo $logo_left['alt']; ?>"
                    />
                </div>
            <?php endif; ?>
            <div class="about-us-block__content-container">
                <?php if (!empty($title)): ?>
                    <div class="about-us-block__title">
                        <?php echo $title; ?>
                    </div>
                <?php endif; ?>
                <?php if (!empty($content)): ?>
                    <div class="about-us-block__content">
                        <?php echo $content; ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php if (!empty($logo_right)): ?>
                <div class="about-us-block__logo about-us-block__logo--right">
                    <img src="<?php echo $logo_right['sizes']['wc-logo-block']; ?>"
                         alt="<?php echo $logo_right['alt']; ?>"
                    />
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>