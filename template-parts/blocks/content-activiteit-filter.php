<?php
$filter_categorie = get_field('selecteer_filter_categorie');

if ($filter_categorie == 'Kinderen') {

    $id = get_term_by('name', 'kinderen', 'product_cat');
    $title = __('School', 'webcommitment-theme');
    $class = '--kinderen';

} else {
    $id = get_term_by('name', 'volwassenen', 'product_cat');
    $title = __('Locatie', 'webcommitment-theme');
    $class = '--volwassenen';
}

$id = $id->term_id;
$terms = get_terms('product_cat', array('parent' => $id, 'hide_empty' => false));
?>

<div class="filter-block filter-block<?php echo $class; ?>">
    <div class="filter-block__filter-row">
        <div class="filter-block__filter-column">
            <div class="filter-block__filter-toggle">
                <?php echo $title; ?>
            </div>
            <div class="filter-block__options">
                <?php
                foreach ($terms as $term):
                    $obj = get_term($term, 'product_cat');
                    ?>
                <?php if ($filter_categorie == 'Kinderen'): ?>
                    <span id="<?php echo $obj->term_id; ?>"
                          class="filter-block__option">
                            <?php echo $obj->name; ?>
                        </span>
                <?php else: ?>
                    <a class="filter-block__option"
                       href="<?php echo get_term_link($obj, 'product_cat'); ?>">
                        <?php echo $obj->name; ?>
                    </a>
                <?php endif; ?>

                <?php
                endforeach;
                ?>
            </div>
        </div>
        <?php if ($filter_categorie == 'Kinderen'):
            $sub_terms = get_terms('product_cat', array('parent' => $id, 'hide_empty' => false));
            foreach ($sub_terms as $sub_term):
                $sub_obj = get_term($sub_term, 'product_cat');
                ?>
                <div
                        id="secondary-<?php echo $sub_term->term_id; ?>"
                        class="filter-block__filter-column secondary-column">
                    <div class="filter-block__filter-toggle filter-block__filter-toggle--secondary">
                        <?php echo __('Groep', 'webcommitment-theme'); ?>
                    </div>
                    <div class="filter-block__options">
                        <?php
                        $child_terms = get_term_children($sub_obj->term_id, 'product_cat');
                        foreach ($child_terms as $child_term):
                            ?>
                            <a href="<?php echo get_term_link($child_term, 'product_cat'); ?>"
                               class="filter-block__option"
                               id="<?php echo $child_term; ?>"
                            >
                                <?php echo get_term($child_term)->name; ?>
                            </a>
                        <?php
                        endforeach;
                        ?>
                    </div>
                </div>

            <?php
            endforeach;
            ?>
        <?php endif; ?>
    </div>
</div>
