<?php
$title = get_field('title');
$quote = get_field('quote');
$content = get_field('content');
?>

<?php if (!empty($title || $quote || $content)): ?>
    <div class="container">
        <div class="row">
            <div class="title-quote-block">
                <div class="title-quote-block__title">
                    <?php echo $title; ?>
                </div>
                <div class="title-quote-block__quote">
                    <?php echo $quote; ?>
                </div>
                <div class="title-quote-block__content">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>