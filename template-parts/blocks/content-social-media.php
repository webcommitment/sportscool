<?php if (have_rows('social_media', 'option')): ?>
    <div class="social-media">
        <?php
        // Loop through rows.
        while (have_rows('social_media', 'option')) :
            the_row();
            $icon = get_sub_field('social_media_icon')['url'];
            $link = get_sub_field('social_media_link');
            ?>
            <div class="social-media__item">
                <a href="<?php echo $link; ?>">
                    <img class="social-media__icon"
                         src="<?php echo $icon ?>"
                         alt="social-media-icon"
                    />
                </a>
            </div>
        <?php
        endwhile; ?>
    </div>
<?php endif; ?>