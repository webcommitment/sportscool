<?php
$featured_posts = get_field('uitgelichte_activiteit');
if ($featured_posts): ?>
    <div class="featured-product-block">
        <div id="featuredProductCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php foreach ($featured_posts as $index => $featured_post): ?>
                    <li data-target="#featuredProductCarousel" data-slide-to="<?php echo $index; ?>"
                        class="<?php echo ($index == 0 ? 'active' : '');?>">
                    </li>
                <?php endforeach; ?>
            </ol>
            <div class="carousel-inner">
                <?php foreach ($featured_posts as $index => $featured_post):
                    $permalink = get_permalink($featured_post->ID);
                    $title = get_the_title($featured_post->ID);
                    ?>
                    <div class="featured-product-block__item carousel-item <?php echo($index == 0 ? 'active' : ''); ?>"
                         style="background-image: url('<?php echo get_the_post_thumbnail_url($featured_post->ID); ?>');"
                    >
                        <div class="featured-product-block__item--inner">
                            <?php echo $title; ?>
                            <a href="<?php echo $permalink; ?>"
                               class="cta-btn cta-btn__blue"
                            >
                                <?php echo __('Lees meer', 'webcommitment-theme'); ?>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>