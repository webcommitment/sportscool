<?php
// Get pickup points
$terms = get_terms([
    'taxonomy' => 'locatie',
    'hide_empty' => false,
]);
?>
<?php if (!empty($terms)): ?>
    <div class="locations-block">
        <?php
        foreach ($terms as $term) :
            $address = get_field('google_map', $term);
            ?>
            <div class="locations-block__item">
                <span class="cta-btn cta-btn__blue">
                    <?php
                    echo $term->name;
                    ?>
                </span>
                <div class="locations-block__tooltip">
                    <?php print_r($address['address']); ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>