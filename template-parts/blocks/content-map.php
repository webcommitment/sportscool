<?php
wp_enqueue_script(
    'sportscool-google-maps',
    'https://maps.googleapis.com/maps/api/js?key=AIzaSyBQao9HGr8jAsvYOVko7T3LdBoIYv0vH5s&callback=locationsMap',
    array( 'jquery' ),
    null,
    true
);
?>
<section class="locations-map">
    <div id="locations-map"></div>
    <script>
        function locationsMap() {
            <?php
            // Get pickup points
            $terms = get_terms([
                'taxonomy' => 'locatie',
                'hide_empty' => false,
            ]);
            ?>

            var locations = [
                <?php
                foreach ( $terms as $term) :

                $cords = get_field('google_map', $term);

                if ( !empty($cords) ) : ?>
                {
                    lat: <?php echo $cords['lat']; ?>,
                    lng: <?php echo $cords['lng']; ?>,
                    address: "<?php echo $cords['address']; ?>"
                },
                <?php
                endif;
                endforeach;
                ?>
            ];

            // The map
            var map = new google.maps.Map(
                document.getElementById('locations-map'), {zoom: 4, disableDefaultUI: true});
            var bounds = new google.maps.LatLngBounds();
            var infowindow = new google.maps.InfoWindow();

            for (i = 0; i < locations.length; i++) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
                    map: map
                });

                //extend the bounds to include each marker's position
                bounds.extend(marker.position);

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i].address);
                        infowindow.open(map, marker);

                        var inputs = document.querySelectorAll('input[name="pickup"]');
                        var inputsLength = inputs.length;

                        for (var j = 0; j < inputsLength; ++j) {
                            var decodeHTML = function (html) {
                                var txt = document.createElement('textarea');
                                txt.innerHTML = html;
                                return txt.value;
                            };
                            var decodedAddress = decodeHTML(locations[i].address);
                            if (inputs[j].value == decodedAddress) {
                                inputs[j].checked = true;
                            }
                        }
                    }
                })(marker, i));
            }

            map.fitBounds(bounds);
        }
    </script>
</section>