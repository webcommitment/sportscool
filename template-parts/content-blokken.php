<?php
$color               = 'orange';
$user_id             = get_current_user_id();
$registration_open   = false;
$registration_closed = false;
$edities_unfiltered  = get_field( 'edities' );

$edities = array_filter( $edities_unfiltered,
	function ( $item ) {
		return $item['eind_datum_en_tijd'] == datetime_is_in_future( $item['eind_datum_en_tijd'] );
	} );

$first_active_edition = array_key_first( $edities );


$available_places   = get_available_places( get_the_ID(), array_key_first( $edities ) );
$user_has_signed_up = user_has_signed_up( $user_id, get_the_ID(), array_key_first( $edities ) );
$begin_date_time    = $edities[ array_key_first( $edities ) ]['begin_datum_en_tijd'];
$end_date_time      = $edities[ array_key_first( $edities ) ]['eind_datum_en_tijd'];
$registration_start = $edities[ array_key_first( $edities ) ]['inschrijving_start_op'];
$registration_end   = $edities[ array_key_first( $edities ) ]['inschrijving_eindigt_op'];
$user_credits       = get_user_meta( $user_id, 'user_credits', true );

$inputDateTime = new DateTime( $begin_date_time );
$inputDateTime->sub( new DateInterval( 'PT1H' ) );
$signup_end_datetime = $inputDateTime->format( 'd-m-Y H:i' );

if ( $available_places > 0 && datetime_is_in_future( $begin_date_time ) && datetime_is_in_future( $registration_start ) == false ) {
	if ( datetime_is_in_future( $signup_end_datetime ) ) {
		$registration_open = true;
	} else {
		$registration_closed = true;
	}
}

$logged_in = false;
if ( is_user_logged_in() ) {
	$logged_in = true;
}
?>
<div class="single-product">
    <article class="product">
        <header class="entry-header">
            <h1 class="entry-title"><?php the_title(); ?></h1>
        </header>
        <h3><?php echo __( 'Eerstvolgende les', 'webcommitment-theme' ); ?></h3>
        <div class="row">
            <div class="col-sm-6">
                <div id="<?php echo array_key_first( $edities ) ?>" class="product-info-block">
                    <div class="product-info-block__book-btn">
						<?php if ( $registration_open ): ?>
							<?php if ( ! $logged_in ): ?>
                                <a href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>"
                                   class="button-primary">
                                                                            <span>
                                                                                <?php echo __( 'Inloggen / Registreren',
	                                                                                'webcommitment-theme' ); ?>
                                                                            </span>
                                </a>
							<?php elseif ( $user_has_signed_up ): ?>
                                <button disabled class="not-active button-primary">
                                    <span>
                                        <?php echo __( 'U bent al ingeschreven', 'webcommitment-theme' ); ?>
                                    </span>
                                </button>
								<?php if ( datetime_is_in_future( $signup_end_datetime ) ): ?>
                                    <button id="unregisterFromLesson" class="cta-btn cta-btn__error"
                                            data-uid="<?php echo get_current_user_id(); ?>"
                                            data-lid="<?php echo get_the_ID(); ?>"
                                            data-unregister="true"
                                            data-edition="<?php echo array_key_first( $edities ) ?>">
										<?php echo __( 'Schrijf je uit', 'webcommitment-theme' ); ?>
                                    </button>
								<?php endif; ?>
							<?php else: ?>
								<?php if ( datetime_is_in_future( $signup_end_datetime ) ): ?>
                                    <button id="registerToLesson" class="button-primary"
                                            data-uid="<?php echo get_current_user_id(); ?>"
                                            data-lid="<?php echo get_the_ID(); ?>"
                                            data-edition="<?php echo array_key_first( $edities ) ?>">
										<?php echo __( 'Schrijf je in', 'webcommitment-theme' ); ?>
                                    </button>
								<?php endif; ?>
							<?php endif; ?>
						<?php elseif ( $registration_closed ): ?>
                            <button disabled class="button-primary not-active">
                                <span><?php echo __( 'Inschrijving beëindigd', 'webcommitment-theme' ); ?></span>
                            </button>
						<?php else: ?>
                            <button disabled class="button-primary not-active">
                                <span><?php echo __( 'Inschrijving nog niet gestart', 'webcommitment-theme' ); ?></span>
                            </button>
						<?php endif; ?>
                    </div>
                    <div class="product-info-block__product-data">
                        <div class="product-info-block__date">
                        <span class="label">
                            <?php echo __( 'Aanvang:', 'webcommitment-theme' ); ?>
                        </span>
							<?php if ( ! empty( $edities[ array_key_first( $edities ) ]['begin_datum_en_tijd'] ) ): ?>
                                <span class="value">
                                <?php echo $edities[ array_key_first( $edities ) ]['begin_datum_en_tijd']; ?>
                                -
                                <?php echo $edities[ array_key_first( $edities ) ]['eind_datum_en_tijd']; ?>
                            </span>
							<?php endif; ?>
                        </div>
                        <div class="product-info-block__location">
							<?php
							$locatie    = get_field( 'location' )[0];
							$locatie_id = $locatie->ID;
							if ( ! empty( $locatie ) ):
								?>
                                <span class="label">
                            <?php echo __( 'Locatie:', 'webcommitment-theme' ); ?>
                        </span>
                                <span class="value">
                                    <a href="<?php echo get_permalink( $locatie_id ); ?>">
                                        <?php
                                        echo $locatie->post_title;
                                        ?>
                                        </a>
                        </span>
							<?php endif; ?>
                        </div>
                        <div class="product-info-block__price">
                        <span class="label">
                            <?php echo __( 'Kosten:', 'webcommitment-theme' ); ?>
                        </span>
                            <span class="value price-field">
                            <div><?php echo $edities[ array_key_first( $edities ) ]['cost'] ?></div>
                            <div>
                                <?php echo $edities[ array_key_first( $edities ) ]['cost'] > 1 ? 'credits' : 'credit'; ?>
                            </div>
                        </span>
                        </div>
                        <div class="product-info-block__stock">
                            <span class="label">
                                <?php echo __( 'Vrije plaatsen:', 'webcommitment-theme' ); ?>
                            </span>
                            <span class="value">
                                <?php echo $available_places; ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="product-description">
                    <div class="product-description__title">
						<?php echo __( 'Over deze activiteit', 'webcommitment-theme' ); ?>
                    </div>
                    <div class="product-description__content">
						<?php the_content(); ?>
                        <span>
                        <?php echo __( 'Inschrijven kan maximaal t/m: ' ); ?><?php echo $edities[ array_key_first( $edities ) ]['inschrijving_eindigt_op']; ?>
                    </span>
                        <span>
                        <?php echo __( 'Contact: ' ); ?><a
                                    href="mailto:<?php echo get_field( 'contactgegevens' ); ?>"><?php echo get_field( 'contactgegevens' ); ?></a>
                    </span>
                    </div>
                </div>
            </div>
        </div>
		<?php
		$count = count( $edities );
		$j     = 0;
		?>
		<?php
		if ( $count > 1 ): ?>
            <div class="row">
                <div class="col">
                    <section class="product__upcoming">
                        <article class="product__upcoming__lessons">
                            <h3><?php echo __( 'Volgende lessen in dit blok', 'webcommitment-theme' ); ?></h3>
                            <ul>
								<?php while ( have_rows( 'edities' ) ) : the_row();
									$begin_date_time            = get_sub_field( 'begin_datum_en_tijd' );
									$eind_date_time             = get_sub_field( 'eind_datum_en_tijd' );
									$inschrijving_start_op      = get_sub_field( 'inschrijving_start_op' );
									$inschrijving_eindigt_op    = get_sub_field( 'inschrijving_eindigt_op' );
									$cost                       = get_sub_field( 'cost' );
									$edition_id                 = $j;
									$user_has_signed_up_edition = user_has_signed_up( $user_id,
										get_the_ID(),
										$edition_id );
									$inputDateTime              = new DateTime( $begin_date_time );
									$inputDateTime->sub( new DateInterval( 'PT1H' ) );
									$signup_end_datetime = $inputDateTime->format( 'd-m-Y H:i' );
									?>
									<?php if ( datetime_is_in_future( $inschrijving_start_op ) === false && datetime_is_in_future( $inschrijving_eindigt_op ) === true ): ?>
										<?php if ( $j != $first_active_edition ): ?>
                                            <li id="<?php echo $edition_id; ?>"
                                                class="product__upcoming__lessons__item">
                                                <div class="product__upcoming__lessons__item__column">
                                                    <strong>
														<?php echo __( 'Datum en tijd:', 'webcommitment-theme' ); ?>
                                                    </strong>
                                                    <span>
                                                    <?php echo $begin_date_time; ?> - </br><?php echo $eind_date_time; ?>
                                                </span>
                                                </div>
                                                <div class="product__upcoming__lessons__item__column">
													<?php
													$locatie = get_field( 'location' )[0];
													?>
                                                    <strong>
														<?php echo __( 'Locatie:', 'webcommitment-theme' ); ?>
                                                    </strong>
                                                    <span>
                                                    <?php echo $locatie->post_title; ?>
                                                </span>
                                                </div>
                                                <div class="product__upcoming__lessons__item__column">
                                                    <strong>
														<?php echo __( 'Vrije plaatsen:', 'webcommitment-theme' ); ?>
                                                    </strong>
                                                    <span>
                                                    <?php echo get_available_places( get_the_ID(), $edition_id ); ?>
                                                </span>
                                                </div>
                                                <div class="product__upcoming__lessons__item__column">
                                                    <strong>
														<?php echo __( 'Kosten:', 'webcommitment-theme' ); ?>
                                                    </strong>
                                                    <span>
                                                    <?php echo $cost ?><?php echo __( 'credit',
	                                                    'webcommitment-theme' ) . $cost > 1 ? ' credits' : ' credit'; ?>
                                                </span>
                                                </div>
                                                <div class="product__upcoming__lessons__item__column">
                                                    <strong>
														<?php echo __( 'Inschrijven kan maximaal t/m: ',
															'webcommitment-theme' ); ?>
                                                    </strong>
                                                    <span>
                                                    <?php echo $inschrijving_eindigt_op; ?>
                                                </span>
                                                </div>
                                                <div class="product__upcoming__lessons__item__column">
													<?php if ( ! $logged_in ): ?>
                                                        <a href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>"
                                                           class="cta-btn cta-btn__blue">
                                                                            <span>
                                                                                <?php echo __( 'Inloggen / Registreren',
	                                                                                'webcommitment-theme' ); ?>
                                                                            </span>
                                                        </a>
													<?php elseif ( $user_has_signed_up_edition ): ?>
                                                        <button disabled class="not-active cta-btn cta-btn__orange">
                                                        <span>
                                                            <?php echo __( 'U bent al ingeschreven',
	                                                            'webcommitment-theme' ); ?>
                                                        </span>
                                                        </button>
														<?php if ( datetime_is_in_future( $signup_end_datetime ) ): ?>
                                                            <button id="unregisterFromLesson"
                                                                    class="cta-btn cta-btn__error"
                                                                    data-uid="<?php echo get_current_user_id(); ?>"
                                                                    data-lid="<?php echo get_the_ID(); ?>"
                                                                    data-unregister="true"
                                                                    data-edition="<?php echo $edition_id; ?>">
																<?php echo __( 'Schrijf je uit',
																	'webcommitment-theme' ); ?>
                                                            </button>
														<?php endif; ?>
													<?php else: ?>
														<?php if ( datetime_is_in_future( $signup_end_datetime ) ): ?>
                                                            <button id="registerToLesson"
                                                                    class="cta-btn cta-btn__orange"
                                                                    data-uid="<?php echo get_current_user_id(); ?>"
                                                                    data-lid="<?php echo get_the_ID(); ?>"
                                                                    data-edition="<?php echo $edition_id; ?>">
																<?php echo __( 'Schrijf je in',
																	'webcommitment-theme' ); ?>
                                                            </button>
														<?php else: ?>
                                                            <button disabled class="button-primary not-active">
                                                                <span><?php echo __( 'Inschrijving beëindigd',
																		'webcommitment-theme' ); ?></span>
                                                            </button>
														<?php endif; ?>
													<?php endif; ?>
                                                </div>
                                            </li>
										<?php
										endif;
									endif;
									$j ++;
									?>
								<?php endwhile; ?>
                            </ul>
                        </article>
                    </section>
                </div>
            </div>
		<?php endif; ?>
    </article>
</div>