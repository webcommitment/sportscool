<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */
$grouped_lessons = get_grouped_lessons_by_date();
$user_id         = get_current_user_id();

get_header();

$logged_in = false;
if (is_user_logged_in()) {
    $logged_in = true;
}

?>
    <article id="archive">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-11">
                        <h1>
							<?php echo __( 'Blokken', 'webcommitment-theme' ); ?>
                        </h1>
                    </div>
                </div>
                <div class="row justify-content-center single-product">
                    <div class="col-11">
						<?php
						foreach ( $grouped_lessons as $month_year => $lessons ) :
							echo '<h2>' . $month_year . '</h2>'; ?>
                            <div class="row">
                                <div class="col">
                                    <article class="product">
                                        <section class="product__upcoming">
                                            <article class="blokken">
                                                <ul>
													<?php foreach ( $lessons as $lesson ) : ?>
														<?php
														$begin_date_time            = $lesson['start_datetime'];
														$eind_date_time             = $lesson['end_datetime'];
														$inschrijving_start_op      = $lesson['inschrijving_start_op'];
														$inschrijving_eindigt_op    = $lesson['inschrijving_eindigt_op'];
														$cost                       = $lesson['cost'];
														$user_has_signed_up_edition = user_has_signed_up( $user_id,
															$lesson['ID'],
															$lesson['index'] );
														?>
														<?php if ( ! datetime_is_in_future( $inschrijving_start_op ) ): ?>
                                                            <div class="product__upcoming__lessons__item__title-block">
                                                                <h3><?php echo get_the_title($lesson['ID']); ?></h3>
                                                                <a href="<?php echo get_permalink($lesson['ID']); ?>"><?php echo __( 'Lees meer',
																		'webcommitment-theme' );?></a>
                                                            </div>
                                                            <li class="product__upcoming__lessons__item">
                                                                <div class="product__upcoming__lessons__item__column">
                                                                    <strong>
																		<?php echo __( 'Datum en tijd:',
																			'webcommitment-theme' ); ?>
                                                                    </strong>
                                                                    <span>
                                                                        <?php echo $begin_date_time; ?> - <?php echo $eind_date_time; ?>
                                                                    </span>
                                                                </div>
                                                                <div class="product__upcoming__lessons__item__column">
																	<?php
																	$locatie    = get_field( 'location' )[0];
																	$locatie_id = $locatie->ID;
																	?>
                                                                    <strong>
																		<?php echo __( 'Locatie:',
																			'webcommitment-theme' ); ?>
                                                                    </strong>
                                                                    <a href="<?php echo get_permalink( $locatie_id ); ?>">
                                                                        <span>
                                                                            <?php echo $locatie->post_title; ?>
                                                                        </span>
                                                                    </a>
                                                                </div>
                                                                <div class="product__upcoming__lessons__item__column">
                                                                    <strong>
																		<?php echo __( 'Vrije plaatsen:',
																			'webcommitment-theme' ); ?>
                                                                    </strong>
                                                                    <span>
                                                                        <?php echo get_available_places( $lesson['ID'],
	                                                                        $lesson['index'] ); ?>
                                                                    </span>
                                                                </div>
                                                                <div class="product__upcoming__lessons__item__column">
                                                                    <strong>
																		<?php echo __( 'Kosten:',
																			'webcommitment-theme' ); ?>
                                                                    </strong>
                                                                    <span>
                                                                        <?php echo $cost ?><?php echo __( ' credit',
                                                                            'webcommitment-theme' ) . $cost > 1 ? 's' : ''; ?>
                                                                    </span>
                                                                </div>
                                                                <div class="product__upcoming__lessons__item__column">
                                                                    <strong>
																		<?php echo __( 'Inschrijven kan maximaal t/m: ',
																			'webcommitment-theme' ); ?>
                                                                    </strong>
                                                                    <span>
                                                                        <?php echo $inschrijving_eindigt_op; ?>
                                                                    </span>
                                                                </div>
                                                                <div class="product__upcoming__lessons__item__column">
                                                                    <?php if (!$logged_in):?>
                                                                        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') );?>" class="cta-btn cta-btn__blue">
                                                                            <span>
                                                                                <?php echo __( 'Inloggen / Registreren',
	                                                                                'webcommitment-theme' ); ?>
                                                                            </span>
                                                                        </a>
																	<?php elseif ( $user_has_signed_up_edition ): ?>
                                                                        <button disabled
                                                                                class="not-active cta-btn cta-btn__orange">
                                                                            <span>
                                                                                <?php echo __( 'U bent al ingeschreven',
	                                                                                'webcommitment-theme' ); ?>
                                                                            </span>
                                                                        </button>
																	<?php else: ?>
                                                                        <button id="registerToLesson"
                                                                                class="cta-btn cta-btn__orange"
                                                                                data-uid="<?php echo get_current_user_id(); ?>"
                                                                                data-lid="<?php echo $lesson['ID']; ?>"
                                                                                data-edition="<?php echo $lesson['index']; ?>">
                                                                            <span>
                                                                                <?php echo __( 'Schrijf je in',
	                                                                                'webcommitment-theme' ); ?>
                                                                            </span>
                                                                        </button>
																	<?php endif; ?>
                                                                </div>
                                                            </li>
														<?php endif; ?>
													<?php endforeach; ?>
                                                </ul>
                                            </article>
                                        </section>
                                    </article>
                                </div>
                            </div>
						<?php endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
    </article>
<?php
get_footer();
