<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package webcommitment_Starter
 */

get_header(); ?>
	<article id="single">
		<section class="main-content">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-11">
						<?php
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content-blokken' );
						endwhile; // End of the loop.
						?>
					</div>
				</div>
			</div>
		</section>
	</article>
<?php
get_footer();
