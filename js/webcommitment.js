(function ($) {

    function parseDate(dateString) {
        var parts = dateString.split(" ");
        var dateParts = parts[0].split("-");
        var timeParts = parts[1].split(":");

        // Note: Months are 0-based in JavaScript Dates
        return new Date(dateParts[2], dateParts[1] - 1, dateParts[0], timeParts[0], timeParts[1]);
    }

    $(document).ready(function () {


        $('#mobileMenuTrigger').click(function () {
            $(this).toggleClass('active');
            $('header .header-container').toggleClass('active-menu');
            $('body').toggleClass('active-menu');
        });

        $(".filter-block__option").click(function () {
            $('.filter-block__option.active').removeClass('active');
            $(this).addClass('active');
            var id = $(this).attr('id');
            var elem = '#secondary-' + id;
            $('.secondary-column.opened').removeClass('opened');
            $(elem).toggleClass("opened");

            $([document.documentElement, document.body]).animate({
                scrollTop: $(".opened").offset().top
            }, 500);
        });


        var blokken = $(".woocommerce .blokken__item");
        if (blokken.length > 0) {
            blokken.sort(function(a, b) {
                var timeA = parseDate($(a).data("sort-time"));
                var timeB = parseDate($(b).data("sort-time"));

                return timeA - timeB;
            });

            // Get the current date and time
            var currentDate = new Date();

            // Separate the blokken__item elements into future and past
            var futureBlokken = blokken.filter(function() {
                var itemDate = parseDate($(this).data("sort-time"));
                return itemDate > currentDate;
            });

            var pastBlokken = blokken.filter(function() {
                var itemDate = parseDate($(this).data("sort-time"));
                return itemDate <= currentDate;
            });

            // Append the future blokken__item elements back to the section
            $(".blokken").html(futureBlokken);

            // Append the past blokken__item elements to another div
            $(".woocommerce .archive-blokken").html(pastBlokken);
        }

    });

    document.addEventListener('DOMContentLoaded', function () {
        new Splide('.splide', {
            "type": "loop",
            "autoplay": true,
            "interval": "3000",
            "perPage": 6,
            breakpoints: {
                992: {perPage: 4},
                640: {perPage: 2},
                480: {perPage: 1}
            }
        }).mount();
    });

    })(jQuery);

