jQuery(document).ready(function ($) {
    const button = $('#registerToLesson');
    const buttonUnregister = $('#unregisterFromLesson');

    button.prop('disabled', false);
    buttonUnregister.prop('disabled', false);

    $(document).on('click', button, function (event) {

        if ($(event.target).is('button#registerToLesson')) {

            event.preventDefault();
            console.log('event target register', $(event.target) );

            const target = $(event.target);
            const userId = target.data('uid');
            const lid = target.data('lid');
            const edition = target.data('edition');
            target.prop('disabled', true)
            target.addClass('not-active');

            $.ajax({
                url: creditsAjax.ajaxurl,
                type: 'POST',
                data: {
                    action: 'check_meta_value',
                    user_id: userId,
                    lesson_id: lid,
                    edition: edition,
                },
                success: function (response) {
                    if (response === '1') {
                        target.html('U bent ingeschreven');
                        target.prop('disabled', true);
                        console.log(response)
                    } else {
                        target.html('Niet genoeg credits');
                        console.log(response)
                    }
                }
            });
        }
    });

    $(document).on('click', buttonUnregister, function (event) {
      
        if ($(event.target).is('button#unregisterFromLesson')) {
            event.preventDefault();
            console.log('event target unregister', $(event.target) );

            const target = $(event.target);
            const userId = target.data('uid');
            const lid = target.data('lid');
            const edition = target.data('edition');
            const unregister = target.data('unregister');
            target.prop('disabled', true)
            target.addClass('not-active');

            $.ajax({
                url: creditsAjax.ajaxurl,
                type: 'POST',
                data: {
                    action: 'check_meta_value',
                    user_id: userId,
                    lesson_id: lid,
                    edition: edition,
                    unregister: unregister,
                },
                success: function (response) {
                    if (response === '1') {
                        target.html('U bent uitgeschreven');
                        target.prop('disabled', true)
                        console.log(response)
                    } else {
                        target.html('Niet genoeg credits');
                        console.log(response)
                    }
                }
            });
        }
    });
});