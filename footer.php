<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webcommitment_Starter
 */

?>
</div><!-- #content -->
</main>
<footer>
    <?php
    get_template_part('template-parts/blocks/content', 'partners');
    ?>
    <div class="footer-container">
        <div class="footer-container__upper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 col-lg-3">
                        <?php
                        $menu_left_name = wp_get_nav_menu_name('footer_left');
                        ?>
                        <div class="footer-container__menu-title">
                            <?php echo $menu_left_name; ?>
                        </div>
                        <div class="footer-container__menu">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'footer_left',
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <?php
                        $menu_right_name = wp_get_nav_menu_name('footer_right');
                        ?>
                        <div class="footer-container__menu-title">
                            <?php echo $menu_right_name; ?>
                        </div>
                        <div class="footer-container__menu">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'footer_right',
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="footer-container__menu-title">
                            <?php echo __('Locaties','webcommitment-theme'); ?>
                        </div>
                        <?php
                            get_template_part('template-parts/blocks/content', 'map');
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-container__lower">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="footer-right-container">
                            <div class="white-logo">
                                <img class="white-logo__image"
                                     src="<?php echo get_field('white_logo', 'option')['sizes']['wc-logo-block']; ?>"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 al-right">
                        <div class="contact-info">
                            <div class="contact-info__address">
                                <span class="label"><?php echo __('Adres', 'webcommitment-theme'); ?></span>
                                <span class="value"><?php echo get_field('address', 'option'); ?></span>
                            </div>
                            <div class="contact-info__e-mail">
                                <span class="label"><?php echo __('E-mail', 'webcommitment-theme'); ?></span>
                                <a class="value" href="mailto:<?php echo get_field('e-mail', 'option'); ?>">
                                    <?php echo get_field('e-mail', 'option'); ?>
                                </a>
                            </div>
                            <div class="contact-info__tel">
                                <span class="label"><?php echo __('Tel:', 'webcommitment-theme'); ?></span>
                                <div class="numbers">
                                    <a class="value" href="tel:<?php echo get_field('telephone', 'option'); ?>">
                                        <?php echo get_field('telephone', 'option'); ?>
                                    </a>
                                    <?php echo __('of', 'webcommitment-theme'); ?>
                                    <a class="value" href="tel:<?php echo get_field('telephone_2', 'option'); ?>">
                                        <?php echo get_field('telephone_2', 'option'); ?>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <?php get_template_part('template-parts/blocks/content','social-media'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
