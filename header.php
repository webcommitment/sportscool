<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webcommitment_Starter
 */
$credits = null;
if (is_user_logged_in()) {
	$customer_id = get_current_user_id();
	$credits = get_user_meta( $customer_id, 'user_credits', true );
	$my_account_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
}
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!--    get analytics script-->
    <?php get_template_part('template-parts/analytics/content', 'head'); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!--    get analytics script-->
<?php get_template_part('template-parts/analytics/content', 'body'); ?>
<div id="page" class="site">
    <header id="masthead" class="site-header" role="banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="header-container">
                        <nav id="site-navigation" class="main-navigation">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'menu-1',
                                'menu_id' => 'primary-menu',
                            ));
                            ?>
                        </nav><!-- #site-navigation -->
                        <h1 class="site-logo">
                            <?php the_custom_logo(); ?>
                        </h1>
                        <nav id="site-navigation" class="main-navigation-right">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'primary_menu_right',
                            ));
                            ?>
                        </nav>
                        <div id="mobileMenuTrigger" class="hamburger-container">
                            <div class="bar1"></div>
                            <div class="bar2"></div>
                            <div class="bar3"></div>
                        </div>
                        <nav id="mobile-navigation" class="main-navigation">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'mobile_menu',
                            ));
                            ?>
                        </nav>
                        <?php if ($credits != null): ?>
                            <div class="credits-count">
                                <a href="<?php echo $my_account_url; ?>"><?php echo __( 'Mijn credits:', 'webcommimtent-theme' ) . ' ' . get_user_meta( $customer_id, 'user_credits', true ) ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </header><!-- #masthead -->
    <main>
        <div id="content" class="site-content">
