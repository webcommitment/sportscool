<?php
// Add custom user meta field for credits
function add_credits_meta_field() {
	add_user_meta('user', 'user_credits', 0, true);
}
add_action('init', 'add_credits_meta_field');

// Display user credits in profile
// Display editable user credits field in profile
function display_editable_user_credits_field($user) {
	$user_credits = get_user_meta($user->ID, 'user_credits', true);
	?>
	<h3>Credits</h3>
	<table class="form-table">
		<tr>
			<th><label for="user_credits">Credits</label></th>
			<td>
				<input type="number" name="user_credits" id="user_credits" value="<?php echo esc_attr($user_credits); ?>" class="regular-text" />
			</td>
		</tr>
	</table>
	<?php
}
add_action('show_user_profile', 'display_editable_user_credits_field');
add_action('edit_user_profile', 'display_editable_user_credits_field');

// Save user credits when profile is updated
// Save updated user credits when profile is updated
function save_editable_user_credits_field($user_id) {
	if (current_user_can('edit_user', $user_id)) {
		if (isset($_POST['user_credits'])) {
			$new_credits = intval($_POST['user_credits']);
			update_user_meta($user_id, 'user_credits', $new_credits);
		}
	}
}
add_action('personal_options_update', 'save_editable_user_credits_field');
add_action('edit_user_profile_update', 'save_editable_user_credits_field');
?>