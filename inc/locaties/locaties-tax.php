<?php
add_action( 'init', 'locaties_product_taxonomy' );
function locaties_product_taxonomy()  {
    $labels = array(
        'name'                       => 'Locaties',
        'singular_name'              => 'Locatie',
        'menu_name'                  => 'Locaties',
        'all_items'                  => 'All Locaties',
        'parent_item'                => 'Parent Locatie',
        'parent_item_colon'          => 'Parent Locatie:',
        'new_item_name'              => 'New Locatie Name',
        'add_new_item'               => 'Add New Locatie',
        'edit_item'                  => 'Edit Locatie',
        'update_item'                => 'Update Locatie',
        'separate_items_with_commas' => 'Separate Locatie with commas',
        'search_items'               => 'Search Locaties',
        'add_or_remove_items'        => 'Add or remove Locaties',
        'choose_from_most_used'      => 'Choose from the most used Locaties',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'locatie', 'product', $args );
    register_taxonomy_for_object_type( 'locatie', 'product' );
}