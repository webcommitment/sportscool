<?php
/**
 * Additional features to allow styling of the templates
 *
 * @package webcommitment_Starter
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 *
 * @return array
 */
function webcommitment_starter_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}

add_filter( 'body_class', 'webcommitment_starter_body_classes' );

/**
 * Register menus
 */

function webcommitment_register_menu() {
	register_nav_menu( 'primary_menu_right', __( 'Primary menu right', 'webcommitment-theme' ) );
	register_nav_menu( 'mobile_menu', __( 'Mobile menu', 'webcommitment-theme' ) );
	register_nav_menu( 'footer_left', __( 'Footer left menu', 'webcommitment-theme' ) );
	register_nav_menu( 'footer_right', __( 'Footer right menu', 'webcommitment-theme' ) );
}

add_action( 'after_setup_theme', 'webcommitment_register_menu' );


/**
 * WooCommerce options
 *
 */

/**
 * @snippet       WooCommerce: Disable Zoom, Lightbox and Product Gallery Slider @ Single Product Page
 * @author        Sandesh Jangam
 * @donate $7     https://www.paypal.me/SandeshJangam/7
 */

add_action( 'wp', 'ts_remove_zoom_lightbox_gallery_support', 99 );

function ts_remove_zoom_lightbox_gallery_support() {
	remove_theme_support( 'wc-product-gallery-zoom' );
	remove_theme_support( 'wc-product-gallery-lightbox' );
	remove_theme_support( 'wc-product-gallery-slider' );
}

/**
 * ACF Functions
 */

add_action( 'acf/init', 'my_acf_init' );
function my_acf_init() {

	// check function exists
	if ( function_exists( 'acf_register_block' ) ) {

		acf_register_block( array(
			'name'            => 'page-header',
			'title'           => __( 'Page header block' ),
			'description'     => __( 'Custom page header block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'format-image',
			'keywords'        => array( 'header', 'image' ),
		) );

		acf_register_block( array(
			'name'            => 'title-quote',
			'title'           => __( 'Title + Quote block' ),
			'description'     => __( 'Custom Title + Quote block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'heading',
			'keywords'        => array( 'title', 'quote' ),
		) );

		acf_register_block( array(
			'name'            => 'product-categories',
			'title'           => __( 'Product categories' ),
			'description'     => __( 'Custom product categories block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'category',
			'keywords'        => array( 'category', 'categories' ),
		) );

		acf_register_block( array(
			'name'            => 'featured-products',
			'title'           => __( 'Featured products' ),
			'description'     => __( 'Custom featured products block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'product',
			'keywords'        => array( 'featured', 'products' ),
		) );

		acf_register_block( array(
			'name'            => 'about-us',
			'title'           => __( 'About us block' ),
			'description'     => __( 'Custom about us block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'info',
			'keywords'        => array( 'about', 'us', 'over ons' ),
		) );

		acf_register_block( array(
			'name'            => 'partners',
			'title'           => __( 'Partners block' ),
			'description'     => __( 'Custom partners block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'businessperson',
			'keywords'        => array( 'partners' ),
		) );

		acf_register_block( array(
			'name'            => 'activiteit-filter',
			'title'           => __( 'Activiteit filter block' ),
			'description'     => __( 'Custom Activiteit filter block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'filter',
			'keywords'        => array( 'activiteit', 'filter' ),
		) );

		acf_register_block( array(
			'name'            => 'social-media',
			'title'           => __( 'Social media block' ),
			'description'     => __( 'Social media block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'social-links',
			'keywords'        => array( 'social', 'media' ),
		) );

		acf_register_block( array(
			'name'            => 'map',
			'title'           => __( 'Locations map block' ),
			'description'     => __( 'Locations map block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'marker',
			'keywords'        => array( 'locations', 'map' ),
		) );

		acf_register_block( array(
			'name'            => 'locations-block',
			'title'           => __( 'Locations block' ),
			'description'     => __( 'Locations block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'marker',
			'keywords'        => array( 'locations' ),
		) );

		acf_register_block( array(
			'name'            => 'background-block',
			'title'           => __( 'Content with background block' ),
			'description'     => __( 'Content with background block.' ),
			'render_callback' => 'my_acf_block_render_callback',
			'category'        => 'widgets',
			'icon'            => 'paragraph',
			'keywords'        => array( 'text', 'content', 'background' ),
		) );
	}
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function editor_style_setup() {
	// Add support for editor styles.
	add_theme_support( 'editor-styles' );

	// Enqueue editor styles.
	add_editor_style( 'editor/styles/style.css' );
}

add_action( 'after_setup_theme', 'editor_style_setup' );

function my_acf_block_render_callback( $block ) {
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace( 'acf/', '', $block['name'] );
	// include a template part from within the "template-parts/block" folder
	if ( file_exists( get_theme_file_path( "/template-parts/blocks/content-{$slug}.php" ) ) ) {
		include( get_theme_file_path( "/template-parts/blocks/content-{$slug}.php" ) );
	}
}

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page();
}

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

// Remove the result count from WooCommerce
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );


/**
 * Image sizes
 */

add_image_size( 'wc-page-header', 1920, 540, true );
add_image_size( 'wc-logo-block', 305, 184 );

/**
 * Custom Post Types and taxonomy registration
 */


require get_template_directory() . '/inc/cpt/partners/partners-cpt.php';
require get_template_directory() . '/inc/cpt/locations/locations.php';
require get_template_directory() . '/inc/cpt/sportcoaches/sportcoaches-cpt.php';
require get_template_directory() . '/inc/cpt/blokken/blokken.php';

require get_template_directory() . '/inc/locaties/locaties-tax.php';

custom_post_type_locations();
custom_post_type_blokken();
custom_post_type_sportcoaches();
partners_init();


/**
 * Custom functions
 */

require get_template_directory() . '/inc/user/user-functions.php';

function so_20990199_product_query( $query ) {
	$query->set( 'post_status', array( 'publish', 'future' ) );
	$query->set( 'order', 'DESC' );
}

add_action( 'woocommerce_product_query', 'so_20990199_product_query' );

function my_media_article_category_query( $query ) {
	if ( is_tax( 'product_cat' ) ) {
		$query->set( 'post_status', array( 'publish', 'future' ) );
		$query->set( 'order', 'DESC' );
	}
}

add_filter( 'pre_get_posts', 'my_media_article_category_query' );

function enqueue_credits_scripts() {
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'credits-ajax-script',
		get_template_directory_uri() . '/js/ajax/credits-ajax.js',
		array( 'jquery' ),
		'1.0',
		true );

	wp_localize_script( 'credits-ajax-script', 'creditsAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_enqueue_scripts', 'enqueue_credits_scripts' );

/**
 * get_current_product_category
 * @return string
 */
function get_current_product_category() {
	global $post;
	$terms  = get_the_terms( $post->ID, 'product_cat' );
	$nterms = get_the_terms( $post->ID, 'product_tag' );
	foreach ( $terms as $term ) {
		$product_cat_id   = $term->term_id;
		$product_cat_name = $term->name;
		break;
	}

	return $product_cat_name;
}

/**
 * get_current_location_category
 * @return string
 */
function get_current_location_category() {
	global $post;
	$terms = get_the_terms( $post->ID, 'locatie' );
	foreach ( $terms as $term ) {
		$product_location_name = $term->name;
		break;
	}

	return $product_location_name;
}

/**
 * @param $lesson_id
 * @param $index
 *
 * @return int
 */
function get_available_places( $lesson_id, $index ) {
	$capacity  = (int) get_field( 'capaciteit_per_les', $lesson_id );
	$edities   = get_field( 'edities', $lesson_id );
	$attendees = ! empty( $edities[ $index ]['deelnemers'] ) ? count( $edities[ $index ]['deelnemers'] ) : 0;

	return $capacity - $attendees;
}

/**
 * @param string $datetime A datetime string (e.g., 'Y-m-d H:i')
 *
 * @return bool
 * @throws Exception
 */
function datetime_is_in_future($datetime) {
	$wordpress_timezone = get_option('timezone_string');
	if (!empty($wordpress_timezone)) {
		$timezone_object = new DateTimeZone($wordpress_timezone);
    } else {
		$timezone_object = new DateTimeZone('Europe/Amsterdam');
    }

	$givenDateTimeObj = new DateTime($datetime, $timezone_object);
	$currentDateTimeObj = new DateTime(null, $timezone_object);

	if ($givenDateTimeObj > $currentDateTimeObj) {
		return true;
	} else {
		return false;
	}
}

/**
 * @param $user_id
 * @param $lesson_id
 * @param $edition
 *
 * @return bool
 */
function user_has_signed_up( $user_id, $lesson_id, $edition ) {
	$edities = get_field( 'edities', $lesson_id );

	if ( ! empty( $edities[ $edition ]['deelnemers'] ) ) {
		return in_array( $user_id, $edities[ $edition ]['deelnemers'] );
	} else {
		return false;
	}
}

function get_grouped_lessons_by_date( $location = null ) {

	if ( $location == null ) {
		$args = array(
			'post_type'      => 'blokken',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'meta_query'     => array(
				array(
					'key'     => 'edities',
					'compare' => 'EXISTS',
				),
			),
		);
	} else {
		$args = array(
			'post_type'      => 'blokken',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'meta_query'     => array(
				'relation' => 'AND',
				array(
					'key'     => 'location',
					'value'   => $location,
					'compare' => 'LIKE',
				),
			),
		);
	}

	$query           = new WP_Query( $args );
	$grouped_lessons = array();

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$lesson_schedule = get_field( 'edities' );

			if ( $lesson_schedule ) {
				foreach ( $lesson_schedule as $index => $schedule_item ) {
					$lesson_start_datetime   = $schedule_item['begin_datum_en_tijd'];
					$lesson_end_datetime     = $schedule_item['eind_datum_en_tijd'];
					$inschrijving_start_op   = $schedule_item['inschrijving_start_op'];
					$inschrijving_eindigt_op = $schedule_item['inschrijving_eindigt_op'];
					$attendees               = $schedule_item['deelnemers'];
					$sportcoach              = $schedule_item['sportcoach'];
					$cost                    = $schedule_item['cost'];
					$mail_sent               = $schedule_item['coach_e-mail_sent'];

					if ( $lesson_start_datetime && ! datetime_is_in_future( $inschrijving_start_op ) ) {
						$month_year                       = date( 'F Y', strtotime( $lesson_start_datetime ) );
						$grouped_lessons[ $month_year ][] = array(
							'ID'                      => get_the_ID(),
							'index'                   => $index,
							'title'                   => get_the_title() . ' (' . $index + 1 . ')',
							'start_datetime'          => $lesson_start_datetime,
							'end_datetime'            => $lesson_end_datetime,
							'inschrijving_start_op'   => $inschrijving_start_op,
							'inschrijving_eindigt_op' => $inschrijving_eindigt_op,
							'deelnemers'              => $attendees,
							'sportcoach'              => $sportcoach,
							'cost'                    => $cost,
							'coach_e-mail_sent'       => $mail_sent,
						);
					}
				}
			}
		}
		wp_reset_postdata();
	}

	// Sort the grouped lessons by start datetime
	foreach ( $grouped_lessons as &$lessons ) {
		usort( $lessons,
			function ( $a, $b ) {
				return strtotime( $a['start_datetime'] ) - strtotime( $b['start_datetime'] );
			} );
	}

	return $grouped_lessons;
}


/**
 * ACF register maps api key
 */

function my_acf_google_map_api( $api ) {
	$api['key'] = 'AIzaSyBQao9HGr8jAsvYOVko7T3LdBoIYv0vH5s';

	return $api;
}

add_filter( 'acf/fields/google_map/api', 'my_acf_google_map_api' );

function add_custom_endpoint() {
	add_rewrite_endpoint( 'mijn-inschrijvingen', EP_PAGES );
}

add_action( 'init', 'add_custom_endpoint' );

function my_posts_where( $where ) {
	$where = str_replace( "meta_key = 'edities_$", "meta_key LIKE 'edities_%", $where );

	return $where;
}

function compare_posts_by_date($post1, $post2) {
	$date1 = strtotime($post1['begin_datum_en_tijd']);
	$date2 = strtotime($post2['begin_datum_en_tijd']);

	// Sort in ascending order
	if ($date1 == $date2) {
		return 0;
	}
	return ($date1 > $date2) ? -1 : 1;
}

add_filter( 'posts_where', 'my_posts_where' );
function custom_page_content() {
	$user_id = get_current_user_id();

	$args = array(
		'post_type'      => 'blokken',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'meta_query'     => array(
			array(
				'key'     => 'edities_$_deelnemers',
				'value'   => '' . $user_id . '',
				'compare' => 'LIKE',
			),
		),
	);

	$query = new WP_Query( $args );
	$posts = array();
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$posts[] =
				array(
					'ID' => get_the_ID(),
				);

		}
	}

	if ( ! empty( $posts ) ) : ?>
        <h1><?php echo __( 'Mijn inschrijvingen', 'webcommimtent-theme' ) ?></h1>
        <section class="blokken">
			<?php
			$e = 0;
				foreach ( $posts as $item ) :
					$editions = get_field( 'edities', $item['ID'] );
					usort($editions, 'compare_posts_by_date');
					foreach ( $editions as $index => $blok ) :

						$inputDateTime = new DateTime($blok['eind_datum_en_tijd']);
						$inputDateTime->sub(new DateInterval('PT2H'));
						$signup_end_datetime = $inputDateTime->format('d-m-Y H:i');

						if ( is_array( $blok['deelnemers'] ) && in_array( $user_id, $blok['deelnemers'] ) ) : ?>
							<?php if ( ! empty( $blok['begin_datum_en_tijd']  ) ): ?>
                                <div class="blokken__item" data-sort-time="<?php echo $blok['begin_datum_en_tijd']; ?>">
									<?php
									$row_index = get_lesson_row_index($item, $blok['begin_datum_en_tijd']);
									$location_id = get_field( 'location', $item['ID'] )[0];
									?>
                                    <h4><?php echo get_the_title( $item['ID'] ) ?></h4>
                                    <div class="blokken__item__details">
                                        <strong><?php echo $blok['begin_datum_en_tijd']; ?>
                                            - <?php echo $blok['eind_datum_en_tijd']; ?></strong>
                                        <span>, <?php echo $location_id->post_title; ?> <a target="_blank"
                                                                                           href="https://www.google.com/maps/search/?api=1&query=<?php echo get_field( 'adres',
											                                                   $location_id->ID ); ?>"> <?php echo get_field( 'adres',
													$location_id->ID ); ?></a></span>
                                        <?php
                                        if(datetime_is_in_future($signup_end_datetime)): ?>
                                            <div class="blokken__item__details__action">
                                            </br>
                                                <button id="unregisterFromLesson" class="cta-btn cta-btn__error"
                                                        data-uid="<?php echo get_current_user_id(); ?>"
                                                        data-lid="<?php echo $item['ID']; ?>"
                                                        data-unregister="true"
                                                        data-edition="<?php echo $row_index; ?>">
                                                    <?php echo __( 'Schrijf je uit', 'webcommitment-theme' ); ?>
                                                </button>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
								<?php
								$e++;
							endif;
						endif; ?>
					<?php endforeach;
				endforeach; ?>
			<?php if ( $e === 0 ): ?>
                <div>
                    <?php echo __( 'Nog geen inschrijvingen. Ga naar ', 'webcommitment-theme' ); ?> <a href="/agenda"><?php echo __( 'Agenda', 'webcommitment-theme' ); ?></a>.
                </div>
			<?php endif; ?>
        </section>
    <h1><?php esc_html_e('Archief', 'webcommitment-theme'); ?></h1>
    <section class="archive-blokken">

    </section>
	<?php endif;
}

add_action( 'woocommerce_account_mijn-inschrijvingen_endpoint', 'custom_page_content' );
function add_custom_page_to_menu( $items ) {
	$items['mijn-inschrijvingen'] = 'Mijn inschrijvingen';

	return $items;
}

add_filter( 'woocommerce_account_menu_items', 'add_custom_page_to_menu', 1 );
function remove_default_menu_items( $items ) {
	unset( $items['dashboard'] );
	unset( $items['orders'] );
	unset( $items['downloads'] );
	unset( $items['edit-account'] );
	unset( $items['customer-logout'] );

	return $items;
}

function get_lesson_row_index($item, $find) {

	$post = get_post($item['ID']);
	$row_index = 1;

	if ($post && get_post_type($post) === 'blokken') {
		$parent_post_id = $post;
		$repeater_field_key = 'edities';
		$repeater_field_value = get_field($repeater_field_key, $parent_post_id);

		foreach ($repeater_field_value as $index => $item) {
			if ($item["begin_datum_en_tijd"] === $find) {
				$row_index = $index;
				break;
			}
		}
	}

	return $row_index;
}

add_filter( 'woocommerce_account_menu_items', 'remove_default_menu_items', 10 );

// Add menu items with your custom order
function add_custom_menu_items( $items ) {
	$new_items = array(
		'dashboard'           => __( 'Dashboard', 'woocommerce' ),
		'mijn-inschrijvingen' => __( 'Mijn inschrijvingen', 'webcommimtent-theme' ),
		'orders'              => __( 'Orders', 'woocommerce' ),
		'downloads'           => __( 'Downloads', 'woocommerce' ),
		'edit-account'        => __( 'Account details', 'woocommerce' ),
		'customer-logout'     => __( 'Logout', 'woocommerce' ),
	);

	return $new_items;
}

add_filter( 'woocommerce_account_menu_items', 'add_custom_menu_items', 20 );

function custom_my_account_content() {
	$args = array(
		'post_type'      => 'product',         // The product post type
		'post_status'    => 'publish',         // Retrieve only published products
		'posts_per_page' => - 1,                // Retrieve all matching products
		'tax_query'      => array(
			array(
				'taxonomy' => 'product_cat',   // The product category taxonomy
				'field'    => 'slug',          // You can also use 'term_id' or 'name'
				'terms'    => 'credits',       // Slug of the "Credits" category
			),
		),
	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) : ?>
        <br/>
        <h3><?php echo __( 'Mijn credits: ', 'webcommimtent-theme' ) ?><?php echo get_field( 'user_credits',
				'user_' . get_current_user_id() ) ?></h3>
        <br/>
        <h2><?php echo __( 'Meer credits kopen: ', 'webcommimtent-theme' ) ?></h2>
        <section class="credits">
			<?php while ( $query->have_posts() ) :
				$query->the_post();
				$add_to_cart_url = wc_get_cart_url();
				?>
                <article class="credits__product">
                    <a href="<?php echo $add_to_cart_url . '?add-to-cart=' . get_the_ID(); ?>"><?php
						the_title(); ?><?php echo __( ' kopen', 'webcommimtent-theme' ) ?></a>
					<?php
					?>
                </article>
			<?php endwhile;
			wp_reset_postdata(); ?>
        </section>
	<?php endif;
}

add_action( 'woocommerce_account_dashboard', 'custom_my_account_content' );

/**
 * Complete WooCommerce orders with credits product.
 */

function custom_credits_payment_complete( $order_id ) {
	$order       = wc_get_order( $order_id );
	$customer_id = get_post_meta( $order_id, '_customer_user', true );
	foreach ( $order->get_items() as $item_id => $item ) {
		$product_id = $item->get_product_id();
		$product    = wc_get_product( $product_id );
		if ( has_term( 'credits', 'product_cat', $product_id ) ) {
			$credits_amount = get_field( 'aantal_credits', $product_id );
			add_user_credits( $customer_id, $credits_amount );
		}
	}
}

add_action( 'woocommerce_order_status_completed', 'custom_credits_payment_complete' );

// Remove the downloads endpoint
function remove_my_account_downloads( $items ) {
    unset( $items['downloads'] );
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'remove_my_account_downloads' );