<?php
function custom_post_type_sportcoaches() {
	$labels = array(
		'name'                  => _x( 'Sport coaches', 'Post type general name', 'webcommitment-theme' ),
		'singular_name'         => _x( 'Sport coach', 'Post type singular name', 'webcommitment-theme' ),
		'menu_name'             => _x( 'Sport coaches', 'Admin Menu text', 'webcommitment-theme' ),
		'name_admin_bar'        => _x( 'Sport coach', 'Add New on Toolbar', 'webcommitment-theme' ),
		'add_new'               => __( 'Add New', 'webcommitment-theme' ),
		'add_new_item'          => __( 'Add New Sport coach', 'webcommitment-theme' ),
		'new_item'              => __( 'New Sport coach', 'webcommitment-theme' ),
		'edit_item'             => __( 'Edit Sport coach', 'webcommitment-theme' ),
		'view_item'             => __( 'View Sport coach', 'webcommitment-theme' ),
		'all_items'             => __( 'All Sport coaches', 'webcommitment-theme' ),
		'search_items'          => __( 'Search Sport coaches', 'webcommitment-theme' ),
		'parent_item_colon'     => __( 'Parent Sport coaches:', 'webcommitment-theme' ),
		'not_found'             => __( 'No Sport coaches found.', 'webcommitment-theme' ),
		'not_found_in_trash'    => __( 'No Sport coaches found in Trash.', 'webcommitment-theme' ),
		'featured_image'        => _x( 'Sport coach Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'webcommitment-theme' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'webcommitment-theme' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'webcommitment-theme' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'webcommitment-theme' ),
		'archives'              => _x( 'Sport coach archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'webcommitment-theme' ),
		'insert_into_item'      => _x( 'Insert into Sport coach', 'Overrides the “Insert into item”/“Insert into Sport coach” phrase (used when inserting media into a post). Added in 4.4', 'webcommitment-theme' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this Sport coach', 'Overrides the “Uploaded to this Sport coach” phrase (used when viewing media attached to a post). Added in 4.4', 'webcommitment-theme' ),
		'filter_items_list'     => _x( 'Filter Sport coaches list', 'Screen reader text for the filter links heading on the post type listing screen. Added in 4.4', 'webcommitment-theme' ),
		'items_list_navigation' => _x( 'Sport coaches list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Added in 4.4', 'webcommitment-theme' ),
		'items_list'            => _x( 'Sport coaches list', 'Screen reader text for the items list heading on the post type listing screen. Added in 4.4', 'webcommitment-theme' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Sport coaches' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'thumbnail' ),
	);

	register_post_type( 'sportcoaches', $args );
}

?>