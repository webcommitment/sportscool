<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 15/01/2019
 * Time: 14:48
 */

function partners_init() {
    $labels = array(
        'name'               => _x( 'Partners', 'post type general name', 'theme core' ),
        'singular_name'      => _x( 'Partner', 'post type singular name', 'theme core' ),
        'menu_name'          => _x( 'Partners', 'admin menu', 'theme core' ),
        'name_admin_bar'     => _x( 'Partners', 'add new on admin bar', 'theme core' ),
        'add_new'            => _x( 'Create new', 'theme core' ),
        'add_new_item'       => __( 'Nieuwe partner toevoegen', 'theme core' ),
        'new_item'           => __( 'Nieuwe partner', 'theme core' ),
        'edit_item'          => __( 'Edit partner', 'theme core' ),
        'view_item'          => __( 'Bekijk partner', 'theme core' ),
        'all_items'          => __( 'Alle partners', 'theme core' ),
        'search_items'       => __( 'Zoek partners:', 'theme core' ),
        'not_found'          => __( 'Geen partners gevonden', 'theme core' ),
        'not_found_in_trash' => __( 'Geen partners gevonden in de prullenbak', 'theme core' )
    );
    $args   = array(
        'labels'             => $labels,
        'description'        => __( 'Description', 'theme core' ),
        'menu_icon'           => 'dashicons-businessperson',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'partners' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'supports'           => array( 'title', 'thumbnail','page-attributes'),
        'taxonomies'         => false
    );
    register_post_type( 'partners', $args );

}