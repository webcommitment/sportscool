<?php
function custom_post_type_locations() {
	$labels = array(
		'name'                  => _x( 'Locations', 'Post type general name', 'webcommitment-theme' ),
		'singular_name'         => _x( 'Location', 'Post type singular name', 'webcommitment-theme' ),
		'menu_name'             => _x( 'Locations', 'Admin Menu text', 'webcommitment-theme' ),
		'name_admin_bar'        => _x( 'Location', 'Add New on Toolbar', 'webcommitment-theme' ),
		'add_new'               => __( 'Add New', 'webcommitment-theme' ),
		'add_new_item'          => __( 'Add New Location', 'webcommitment-theme' ),
		'new_item'              => __( 'New Location', 'webcommitment-theme' ),
		'edit_item'             => __( 'Edit Location', 'webcommitment-theme' ),
		'view_item'             => __( 'View Location', 'webcommitment-theme' ),
		'all_items'             => __( 'All Locations', 'webcommitment-theme' ),
		'search_items'          => __( 'Search Locations', 'webcommitment-theme' ),
		'parent_item_colon'     => __( 'Parent Locations:', 'webcommitment-theme' ),
		'not_found'             => __( 'No locations found.', 'webcommitment-theme' ),
		'not_found_in_trash'    => __( 'No locations found in Trash.', 'webcommitment-theme' ),
		'featured_image'        => _x( 'Location Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'webcommitment-theme' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'webcommitment-theme' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'webcommitment-theme' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'webcommitment-theme' ),
		'archives'              => _x( 'Location archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'webcommitment-theme' ),
		'insert_into_item'      => _x( 'Insert into location', 'Overrides the “Insert into item”/“Insert into Location” phrase (used when inserting media into a post). Added in 4.4', 'webcommitment-theme' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this location', 'Overrides the “Uploaded to this Location” phrase (used when viewing media attached to a post). Added in 4.4', 'webcommitment-theme' ),
		'filter_items_list'     => _x( 'Filter locations list', 'Screen reader text for the filter links heading on the post type listing screen. Added in 4.4', 'webcommitment-theme' ),
		'items_list_navigation' => _x( 'Locations list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Added in 4.4', 'webcommitment-theme' ),
		'items_list'            => _x( 'Locations list', 'Screen reader text for the items list heading on the post type listing screen. Added in 4.4', 'webcommitment-theme' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'locations' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail' ),
	);

	register_post_type( 'locations', $args );
}


?>