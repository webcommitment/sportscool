<?php
function custom_post_type_blokken() {
	$labels = array(
		'name'                  => _x( 'Blokken', 'Post type general name', 'webcommitment-theme' ),
		'singular_name'         => _x( 'Blok', 'Post type singular name', 'webcommitment-theme' ),
		'menu_name'             => _x( 'Blokken', 'Admin Menu text', 'webcommitment-theme' ),
		'name_admin_bar'        => _x( 'Blok', 'Add New on Toolbar', 'webcommitment-theme' ),
		'add_new'               => __( 'Add New', 'webcommitment-theme' ),
		'add_new_item'          => __( 'Add New Blok', 'webcommitment-theme' ),
		'new_item'              => __( 'New Blok', 'webcommitment-theme' ),
		'edit_item'             => __( 'Edit Blok', 'webcommitment-theme' ),
		'view_item'             => __( 'View Blok', 'webcommitment-theme' ),
		'all_items'             => __( 'All Blokken', 'webcommitment-theme' ),
		'search_items'          => __( 'Search Blokken', 'webcommitment-theme' ),
		'parent_item_colon'     => __( 'Parent Blokken:', 'webcommitment-theme' ),
		'not_found'             => __( 'No blokken found.', 'webcommitment-theme' ),
		'not_found_in_trash'    => __( 'No blokken found in Trash.', 'webcommitment-theme' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'blokken' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail' ),
	);

	register_post_type( 'blokken', $args );
}
?>