<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}
$category = get_current_product_category();
$color = 'orange';
$category == 'Kinderen' ? $color = 'orange' : $color = 'purple';
$stock_status = $product->get_stock_status();
$post_thumbnail = get_the_post_thumbnail_url($product->ID, 'wc-page-header');

?>
<li class="archive-product <?php echo strtolower($category); ?>">
    <div class="product-info-block">
        <?php if($post_thumbnail): ?>
            <div class="product-image">
                <div class="product-image__img"
                    style="background-image: url('<?php echo get_the_post_thumbnail_url($product->ID, 'wc-page-header'); ?>')"
                >
                </div>
            </div>
        <?php endif; ?>
        <div class="product-info-block__product-data">
            <div class="product-info-block__title">
                <?php the_title(); ?>
            </div>
            <div class="product-info-block__date">
                        <span class="label">
                            <?php echo __('Aanvang:', 'webcommitment-theme'); ?>
                        </span>
                <?php if (!empty(get_field('startdatum'))): ?>
                    <span class="value">
                                <?php echo get_field('startdatum'); ?>
                                -
                                <?php echo strstr(get_field('einddatum'), ' '); ?>
                            </span>
                <?php endif; ?>
            </div>
            <div class="product-info-block__location">
                <?php
                $locatie = get_current_location_category();
                if (!empty($locatie)):
                    ?>
                    <span class="label">
                            <?php echo __('Locatie:', 'webcommitment-theme'); ?>
                        </span>
                    <span class="value">
                            <?php
                            echo $locatie;
                            ?>
                        </span>
                <?php endif; ?>
            </div>
            <div class="product-info-block__price">
                        <span class="label">
                            <?php echo __('Kosten:', 'webcommitment-theme'); ?>
                        </span>
                <span class="value price-field">
                            <div><?php echo woocommerce_template_single_price(); ?></div>
                            <div>
                                <?php echo __('p/p', 'webcommitment-theme'); ?>
                            </div>
                        </span>
            </div>
	        <?php if($stock_status == 'outofstock'):  ?>
                <div class="product-info-block__stock">
	                <span style="color: #8f1919; font-weight: bold;"><?php echo __('Uitverkocht', 'webcommitment-theme'); ?></span>
                </div>
            <?php endif; ?>
        </div>
	    <?php if($stock_status != 'outofstock'):  ?>
        <a href="<?php echo get_permalink(); ?>" class="product-info-block__read-more">
            <?php echo __('Lees meer', 'webcommitment-theme'); ?>
        </a>
	    <?php endif; ?>
        <div class="product-info-block__book-btn">
            <?php if (get_post_status() != 'future') : ?>
                <?php echo woocommerce_simple_add_to_cart(); ?>
            <?php endif; ?>
        </div>
    </div>
</li>
