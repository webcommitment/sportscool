<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}

?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class('', $product); ?> >

    <?php
    /**
     * Hook: woocommerce_before_single_product_summary.
     *
     * @hooked woocommerce_show_product_sale_flash - 10
     * @hooked woocommerce_show_product_images - 20
     */
    do_action('woocommerce_before_single_product_summary');
    ?>

    <div class="row">
        <div class="col-sm-6">
            <div class="product-info-block">
                <div class="product-info-block__book-btn">
                    <?php if (get_post_status() != 'future') : ?>
                        <?php echo woocommerce_simple_add_to_cart(); ?>
                    <?php else: ?>
                        <button disabled class="button-primary not-active">
                            <span><?php echo __('Inschrijving nog niet gestart', 'webcommitment-theme'); ?></span>
                        </button>
                    <?php endif; ?>
                </div>
                <div class="product-info-block__product-data">
                    <div class="product-info-block__date">
                        <span class="label">
                            <?php echo __('Aanvang:', 'webcommitment-theme'); ?>
                        </span>
                        <?php if (!empty(get_field('startdatum'))): ?>
                            <span class="value">
                                <?php echo get_field('startdatum'); ?>
                                -
                                <?php echo strstr(get_field('einddatum'), ' '); ?>
                            </span>
                        <?php endif; ?>
                    </div>
                    <div class="product-info-block__location">
                        <?php
                        $locatie = get_current_location_category();
                        if (!empty($locatie)):
                            ?>
                            <span class="label">
                            <?php echo __('Locatie:', 'webcommitment-theme'); ?>
                        </span>
                            <span class="value">
                            <?php
                            echo $locatie;
                            ?>
                        </span>
                        <?php endif; ?>
                    </div>
                    <div class="product-info-block__price">
                        <span class="label">
                            <?php echo __('Kosten:', 'webcommitment-theme'); ?>
                        </span>
                        <span class="value price-field">
                            <div><?php echo woocommerce_template_single_price(); ?></div>
                            <div>
                                <?php echo __('p/p', 'webcommitment-theme'); ?>
                            </div>
                        </span>
                    </div>
                    <div class="product-info-block__stock">
                        <span class="label">
                            <?php echo __('Plaatsen:', 'webcommitment-theme'); ?>
                        </span>
                        <span class="value">
                            <?php echo wc_get_stock_html( $product ); ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="product-description">
                <div class="product-description__title">
                    <?php echo __('Over deze activiteit', 'webcommitment-theme'); ?>
                </div>
                <div class="product-description__content">
                    <?php the_content(); ?>
                    <span>
                        <?php echo __('Inschrijven kan maximaal t/m: '); ?><?php echo get_field('laatste_inschrijvingen_op'); ?>
                    </span>
                    <span>
                        <?php echo __('Contact: '); ?><a
                                href="mailto:<?php echo get_field('contactgegevens'); ?>"><?php echo get_field('contactgegevens'); ?></a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>