<?php
/**
 * Webcommitment Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package webcommitment_Starter
 */

if ( ! function_exists( 'webcommitment_starter_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function webcommitment_starter_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Webcommitment Theme, use a find and replace
		 * to change 'webcommitment-theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'webcommitment-theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		* Enable support for Site Custom logo on posts and pages.
		*
		* @link https://codex.wordpress.org/Theme_Logo
		*/
		add_theme_support( 'custom-logo' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'webcommitment-theme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5',
			array(
				'search-form',
				'gallery',
				'caption',
			) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background',
			apply_filters( 'webcommitment_starter_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
add_action( 'after_setup_theme', 'webcommitment_starter_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function webcommitment_starter_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'webcommitment_starter_content_width', 640 );
}

add_action( 'after_setup_theme', 'webcommitment_starter_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function webcommitment_starter_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'webcommitment-theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'webcommitment-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

add_action( 'widgets_init', 'webcommitment_starter_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function webcommitment_starter_scripts() {
	wp_enqueue_style( 'webcommitment-theme-style-bootstrap',
		'https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css' );
	wp_enqueue_script( 'webcommitment-theme-script-popper',
		'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js',
		array( 'jquery' ),
		1.12,
		true );
	wp_enqueue_script( 'webcommitment-theme-script-bootstrap',
		'https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js',
		array( 'jquery' ),
		4.0,
		true );


	wp_enqueue_style( 'webcommitment-theme-style', get_template_directory_uri() . '/dist/css/style.css' );
	wp_enqueue_style( 'webcommitment-splide-style',
		'https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css' );

	wp_enqueue_script( 'webcommitment-theme-script-min',
		get_template_directory_uri() . '/dist/js/main.min.js',
		array( 'jquery' ),
		null,
		true );

	wp_enqueue_script( 'webcommitment-splide',
		'https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js',
		array( 'jquery' ),
		null,
		true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'webcommitment_starter_scripts' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function webcommitment_starter_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}

add_action( 'wp_head', 'webcommitment_starter_pingback_header' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Additional features to allow styling of the templates.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );

/**
 * Disable comments all together.
 */
require get_template_directory() . '/inc/disable-comments.php';

//Replace style-login.css with the name of your custom CSS file
function my_custom_login_stylesheet() {
	wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/dist/css/style.css' );
}

//This loads the function above on the login page
add_action( 'login_enqueue_scripts', 'my_custom_login_stylesheet' );

add_filter( 'woocommerce_get_availability', 'wcs_custom_get_availability', 1, 2 );
function wcs_custom_get_availability( $availability, $_product ) {
	if ( $_product->is_in_stock() ) {
		$availability['availability'] = $_product->get_stock_quantity() . ' ' . __( 'Beschikbaar', 'woocommerce' );
	}
	// Change Out of Stock Text
	if ( ! $_product->is_in_stock() ) {
		$availability['availability'] = __( 'Niet beschikbaar', 'woocommerce' );
	}

	return $availability;
}

/**
 * @param $user_id
 * @param $value
 */
function deduct_user_credits( $user_id, $value ) {
	$user_credits = (int) get_user_meta( $user_id, 'user_credits', true );
	$new_credits  = $user_credits - (int) $value;
	update_user_meta( $user_id, 'user_credits', $new_credits, $user_credits );

	return;
}

function add_user_credits( $user_id, $value ) {
	$value        = (int) $value;
	$user_credits = (int) get_user_meta( $user_id, 'user_credits', true );
	$new_credits  = $user_credits + $value;
	update_user_meta( $user_id, 'user_credits', $new_credits, $user_credits );

	return;
}

/**
 * @param $lesson_id
 * @param $edition
 * @param $user_id
 */
function add_user_to_lesson_attendees( $lesson_id, $edition, $user_id ) {
	$edities   = get_field( 'edities', $lesson_id );
	$attendees = $edities[ $edition ]['deelnemers'];
	if ( ! empty( $attendees ) ) {
		array_push( $attendees, $user_id );
	} else {
		$attendees = array( $user_id );
	}
	update_post_meta( $lesson_id, 'edities_' . $edition . '_deelnemers', $attendees );

	return;
}

/**
 * @param $lesson_id
 * @param $edition
 * @param $user_id
 */
function remove_user_from_lesson_attendees( $lesson_id, $edition, $user_id ) {
	$edities = get_field( 'edities', $lesson_id );

	$attendees = $edities[ $edition ]['deelnemers'];
	if ( ! empty( $attendees ) ) {
		$attendees = array_values( array_filter( $attendees,
			function ( $value ) use ( $user_id ) {
				return $value !== $user_id;
			} ) );
	}
	update_post_meta( $lesson_id, 'edities_' . $edition . '_deelnemers', $attendees );

	return;
}

/**
 *
 */
function check_meta_value_ajax_handler() {
	$user_id     = isset( $_POST['user_id'] ) ? intval( $_POST['user_id'] ) : 0;
	$lesson_id   = isset( $_POST['lesson_id'] ) ? intval( $_POST['lesson_id'] ) : 0;
	$edition     = isset( $_POST['edition'] ) ? intval( $_POST['edition'] ) : 0;
	$unregister  = isset( $_POST['unregister'] );
	$edities     = get_field( 'edities', $lesson_id );
	$lesson_cost = (int) $edities[ $edition ]['cost'];

	if ( $user_id > 0 ) {
		$user_credits = (int) get_user_meta( $user_id, 'user_credits', true );
		if ( $unregister ) {
			remove_user_from_lesson_attendees( $lesson_id, $edition, $user_id );
			add_user_credits( $user_id, $lesson_cost );

			echo true;
		} else {
			if ( $user_credits !== '' && $user_credits >= $lesson_cost ) {
				deduct_user_credits( $user_id, $lesson_cost );
				add_user_to_lesson_attendees( $lesson_id, $edition, $user_id );
			
				echo true;
			} else {
				echo false;
			}
		}
	} else {
		echo 'Invalid User ID.';
	}

	die();
}

add_action( 'wp_ajax_check_meta_value', 'check_meta_value_ajax_handler' );
add_action( 'wp_ajax_nopriv_check_meta_value', 'check_meta_value_ajax_handler' );


function get_message_html($content, $lesson_id, $start, $attendees) {
    return '<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <title>
  </title>
  <!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
    #outlook a {
      padding: 0;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }
  </style>
  <!--[if mso]>
        <noscript>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        </noscript>
        <![endif]-->
  <!--[if lte mso 11]>
        <style type="text/css">
          .mj-outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->
  <style type="text/css">
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100% !important;
        max-width: 100%;
      }
    }
  </style>
  <style media="screen and (min-width:480px)">
    .moz-text-html .mj-column-per-100 {
      width: 100% !important;
      max-width: 100%;
    }
  </style>
  <style type="text/css">
    @media only screen and (max-width:480px) {
      table.mj-full-width-mobile {
        width: 100% !important;
      }

      td.mj-full-width-mobile {
        width: auto !important;
      }
    }
  </style>
</head>

<body style="word-spacing:normal;background-color:#F4F4F4;">
  <div style="background-color:#F4F4F4;">
    <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:30px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                  <tbody>
                    <tr>
                      <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                          <tbody>
                            <tr>
                              <td style="width:214px;">
                                <img height="auto" src="https://sport-s-cool.nl/wp-content/uploads/2020/09/logo.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="214" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:0px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:30px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                  <tbody>
                    <tr>
                      <td align="left" style="font-size:0px;padding:10px 25px;padding-top:10px;padding-bottom:10px;word-break:break-word;">
                        <div style="font-family:Arial, sans-serif;font-size:30px;line-height:22px;text-align:left;color:#444444;">
                          <p style="line-height: 30px; margin: 10px 0; text-align: center; color:#444; font-size:30p; font-family:Arial,sans-serif">Deelnemerslijst Fit`sCool les</p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
    <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:20px 0 40px;padding-bottom:60px;padding-top:0px;text-align:center;">
              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                  <tbody>
                    <tr>
                      <td align="left" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-right:40px;padding-bottom:0px;padding-left:40px;word-break:break-word;">
                        <div style="font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;color:#444444;">
                          <p style="line-height: 16px; margin: 10px 0;font-size:14px; color:#444; font-family:Arial,sans-serif; color:#444">
                          ' . $content . '</br>' . '<strong>Deelnemerslijst voor:</strong> ' . $lesson_id . ', op: ' . $start . '</br></br>' . $attendees . '</br></br>Met vriendelijke groeten,</br></br>Team Sport`s Cool</p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!--[if mso | IE]></td></tr></table><![endif]-->
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--[if mso | IE]></td></tr></table><![endif]-->
  </div>
</body>

</html>';
}

if ( ! wp_next_scheduled( 'send_reminder_email' ) ) {
	wp_schedule_event( current_time( 'timestamp', true ), 'my_minute_interval', 'send_reminder_email' );
}

add_action( 'send_reminder_email', 'send_reminder_email' );

function send_reminder_email() {
	$grouped_lessons = get_grouped_lessons_by_date();
	$current_date_obj    = new DateTime( null, new DateTimeZone( get_option( 'timezone_string' ) ) );
	error_log('send_reminder_email() is launched at: ' . $current_date_obj->format('d-m-Y h:i'));

	foreach ( $grouped_lessons as $month_year => $lessons ) {

		foreach ( $lessons as $lesson ) {
			$lesson_id           = $lesson['ID'];
			$attendees           = $lesson['deelnemers'];
			$inputDateTime = new DateTime( $lesson['start_datetime'] );
			$inputDateTime->sub( new DateInterval( 'PT1H' ) );
			$schedule_date_time = $inputDateTime->format( 'd-m-Y H:i' );
			$sportcoach          = $lesson['sportcoach'];
			$coach_email_content = get_field( 'coach_email_content', 'option' );
			$sportcoach_email    = get_field( 'coach_e-mailadres', $sportcoach );
			$from_email         = get_field( 'coach_email_from', 'option' );
			$post_date_obj       = new DateTime( $schedule_date_time,
				new DateTimeZone( get_option( 'timezone_string' ) ) );
			$time_difference     = $post_date_obj->getTimestamp() - $current_date_obj->getTimestamp();
			$max_time_difference = - 3700;
			if ( $time_difference <= 0 && $time_difference >= $max_time_difference && ( $lesson['coach_e-mail_sent'] === 0 || ! isset( $lesson['coach_e-mail_sent'] ) || $lesson['coach_e-mail_sent'] === false ) ) {

			    $attendee_names = '';
				foreach ( $attendees as $attendee ) {
					$user_data      = get_userdata( $attendee );
					$billing_phone = get_user_meta($user_data->ID, 'billing_phone', true);
					$attendee_name  = $user_data->first_name . ' ' . $user_data->last_name;
					$attendee_names .= $attendee_name . " - <a href='mailto:" . $user_data->user_email ."'>" . "$user_data->user_email" . "</a> - <a href='tel:" . $billing_phone . "'>" . $billing_phone ."</a></br>";
				}
				$to      = $sportcoach_email;
				$subject = 'Deelnemerslijst Fit`sCool les';
				$message = get_message_html($coach_email_content, get_the_title( $lesson_id ), $lesson['start_datetime'], $attendee_names);
				$headers = array('Content-Type: text/html; charset=UTF-8');
				$headers[] = 'From: Sport`s Cool <' . $from_email . '>';
				wp_mail( $to, $subject, $message, $headers );
				$log = ('Cron mail at: ' . $current_date_obj->format('d-m-Y h:i') .' ,sent to: ' . $to . ', with subject: ' . $subject . ', for lesson: ' . get_the_title( $lesson_id ) . ', on: ' . $lesson['start_datetime']);
				error_log($log);
				update_post_meta( $lesson['ID'], 'edities_' . $lesson['index'] . '_coach_e-mail_sent', 1 );
			}
		}
	}
}

function my_minute_interval( $schedules ) {
	$schedules['my_minute_interval'] = array(
		'interval' => 60,
		'display'  => __( 'Every Minute' ),
	);

	return $schedules;
}
add_filter( 'cron_schedules', 'my_minute_interval' );

function add_custom_submenu_page() {
	add_submenu_page(
		'edit.php?post_type=blokken',
		'Les statistieken',
		'Les statistieken',
		'manage_options',
		'statistieken',
		'custom_submenu_callback'
	);
}

function custom_submenu_callback() {
	$args = array(
		'post_type'      => 'blokken',
		'post_status'    => 'publish',
		'posts_per_page' => -1,
	);

	$query = new WP_Query($args);
	$posts = array();
	if ($query->have_posts()) {
		while ($query->have_posts()) {
			$query->the_post();
			$posts[] = array(
				'ID' => get_the_ID(),
			);
		}
	}
	echo '<div class="wrap"><h2>Les statistieken</h2></div></br>';

	function compareEditions($a, $b) {
		$a_participants = is_array($a['deelnemers']) ? count($a['deelnemers']) : 0;
		$b_participants = is_array($b['deelnemers']) ? count($b['deelnemers']) : 0;
		return $b_participants - $a_participants;
	}

	foreach ($posts as $item) {
		$editions = get_field('edities', $item['ID']);

		// Sort the $editions array by 'begin_datum_en_tijd' field
		usort($editions, function($a, $b) {
			$dateA = strtotime($a['begin_datum_en_tijd']);
			$dateB = strtotime($b['begin_datum_en_tijd']);
			return  $dateA - $dateB;
		});

		?>
        <table class="wp-list-table widefat fixed striped">
            <tr>
                <th><strong>Activiteit</strong></th>
                <th><strong>Aanvang</strong></th>
                <th><strong>Aantal</strong></th>
            </tr>
			<?php foreach ($editions as $index => $blok) : ?>
                <tr>
                    <td><?php echo get_the_title($item['ID']) ?></td>
                    <td><?php echo $blok['begin_datum_en_tijd']; ?> - <?php echo $blok['eind_datum_en_tijd']; ?></td>
					<?php if (!empty($blok['deelnemers'])): ?>
                        <td><?php echo count($blok['deelnemers']); ?></td>
					<?php else: ?>
                        <td>0</td>
					<?php endif; ?>
                </tr>
			<?php endforeach; ?>
        </table>
        </br>
		<?php
	}
}


add_action( 'admin_menu', 'add_custom_submenu_page' );

function my_acf_user_field_display_name_and_email( $title, $user, $field, $post_id ) {
    return $title . ' - ' . $user->user_email;
}
add_filter('acf/fields/user/result', 'my_acf_user_field_display_name_and_email', 10, 4);

function my_custom_repeater_column_title_script() {
    ?>
    <script type="text/javascript">
        (function($) {
            function wrapEmails() {
                $('.acf-field-user select').each(function() {
                    var $select = $(this);

                    // Clear any previously added email links
                    $select.siblings('.email-links').remove();

                    var emailLinks = $('<div class="email-links" style="margin-top: 8px;"></div>');

                    $select.find('option:selected').each(function() {
                        var $option = $(this);
                        var text = $option.text().trim();

                        // Extract email from the format "title - email"
                        var email = text.split(' - ')[1];

                        if (email) {
                            var mailtoLink = $('<a href="mailto:' + email + '" style="display: block; margin:7px 0;">' + email + '</a>');
                            emailLinks.append(mailtoLink);
                        }
                    });

                    $select.after(emailLinks);
                });
            }

            function addColumnTitle() {
                $('.acf-field-repeater[data-name="edities"]').each(function() {
                    $(this).find('.acf-row').each(function() {
                        var $row = $(this);

                        // Collect all selected user emails from the select options
                        var emails = [];
                        $row.find('.acf-field-user select option:selected').each(function() {
                            var text = $(this).text().trim();

                            // Extract email from the format "title - email"
                            var email = text.split(' - ')[1];

                            if (email) {
                                emails.push(email);
                            }
                        });

                        // Only add the title element if there are emails
                        if (emails.length > 0) {
                            // Check if the title already exists to avoid duplication
                            if ($row.find('.deelnemers-column-title').length === 0) {
                                // Create the title element with the mailto link
                                var $title = $('<div class="deelnemers-column-title" style="margin: 25px 0 10px;"><a href="mailto:" class="mail-all-deelnemers">Mail alle deelnemers</a></div>');

                                // Insert the title above the "deelnemers" field
                                $row.find('.acf-field[data-name="deelnemers"]').append($title);
                            }

                            // Join emails by comma and update the mailto link
                            var emailString = emails.join(',');
                            $row.find('.mail-all-deelnemers').attr('href', 'mailto:' + emailString);
                        } else {
                            // Remove the title element if no emails are found
                            $row.find('.deelnemers-column-title').remove();
                        }
                    });
                });
            }

            // Hook into the repeater field initialization to add the title and emails
            $(document).on('acf/add', function(e, $el) {
                if ($el.hasClass('acf-field-repeater')) {
                    wrapEmails();
                    addColumnTitle();
                }
            });

            // Run on page load to add the title and emails to existing rows
            $(document).ready(function() {
                wrapEmails();
                addColumnTitle();
            });

            // Run after ACF initialization to ensure all fields are loaded
            $(window).on('load', function() {
                wrapEmails();
                addColumnTitle();
            });

        })(jQuery);
    </script>
    <?php
}

add_action('acf/input/admin_footer', 'my_custom_repeater_column_title_script');


















